<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        Climbers
    </title>
    <meta charset="utf-8">  
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
    <link rel="stylesheet" type="text/css" href="../css/climbers.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
     
</head>

<body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="table_container">
	<table id = "table" class="table table-striped">
	    <h1>
                Climbers
            </h1>
	    <thead>
	        <tr>
		    <td>
		        Name
		    </td>
                    <td>
		        Nationality
		    </td>
		    <td>
                        Achievements
                    </td>
		</tr>
	    </thead>
	    <tbody>
			<?php
			    $host = "fall-2019.cs.utexas.edu";
			    $user = "cs329e_mitra_jaisimha";
			    $pwd = "rump9Easy&suffer";
			    $dbs = "cs329e_mitra_jaisimha";
			    $port = "3306";

			    $connect = mysqli_connect ($host, $user, $pwd, $dbs, $port);
			    $query = "SELECT * FROM Climbers ORDER BY Name;";
			    $result = mysqli_query($connect, $query);
			    while($row = $result->fetch_row())
			    {
				echo("<tr>");
				echo("<td>" . $row[0] . "</td>");
				echo("<td>" . $row[1] . "</td>");
				echo("<td>" . $row[2] . "</td>");
				echo("</tr>");
			    }
                ?>
	    </tbody>
	</table>
        <br><br>
	<p>Don't see your favorite climber? Add them <a href="./add_climber.php">here</a>.</p>
	<br><br>
	<p> Source: <a href="https://en.wikipedia.org/wiki/List_of_climbers_and_mountaineers">Wikipedia</a> </p>
    </div>
    <br><br><br>
</body>
<script>
    $(document).ready(function(){
	    $('#table').dataTable();
    });
</script>

</html>
