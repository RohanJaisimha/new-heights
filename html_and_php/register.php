<?php
    $script = $_SERVER['PHP_SELF'];
    if(isset($_GET["multimedia"]))
	    $script = $script . "?multimedia=true";
    else if(isset($_GET["climber"]))
	    $script = $script . "?climber=true";

    print <<< PAGE
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Register</title>
	    <link rel="stylesheet" type="text/css" href="../css/register.css">
	    <script src="../js/encrypt.js"></script>
        </head>
        <body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
	</div>
	<div id="form_container">
            <form action="$script" method="post" onsubmit="return validate()">
                <table>
                    <h1>Register</h1>
                    <tbody>
                        <tr>
                            <td>Username:</td>
                            <td><input type="text" name="user" id="user"></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input type="password" name="pass" id="pass"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="submit" name="register" value="Register"></td>
                        </tr>
                    </tbody>
                </table>
		</form>
		<div id="error">
		</div>
		</div>
	</body>
        <script>
            function validate()
	    {
		    let error_box = document.getElementById("error");
		    let user_input = document.getElementsByName("user")[0];
		    let pass_input = document.getElementsByName("pass")[0];
		    if(user_input.value == "" || user_input.value.length > 20)
		    {
	                error_box.innerHTML = "<h3>Username should be between 1 & 20 characters (inclusive)</h3>";
			return false;    
		    }
		    if(pass_input.value == "" || pass_input.value.length > 50)
		    {
	                error_box.innerHTML = "<h3>Password should be between 1 & 50 characters (inclusive)</h3>";
		        return false;
		    }
		    pass_input.value = Sha256.hash(pass_input.value); 
		    return true;
	    }
	</script>
    </html>
PAGE;

if (isset($_POST["register"])) {
    register();
}

function register() {
    $host = "fall-2019.cs.utexas.edu";
    $user = "cs329e_mitra_jaisimha";
    $pwd = "rump9Easy&suffer";
    $dbs = "cs329e_mitra_jaisimha";
    $port = "3306";

    $connect = mysqli_connect ($host, $user, $pwd, $dbs, $port);

    $user_in = mysqli_real_escape_string($connect, $_POST["user"]);
    $pass_in = $_POST["pass"];
    if($user_in == "")
    {
	echo("<script>alert(\"Please put in a username\");</script>");
	return;
    }
    if($pass_in == "")
    {
	echo("<script>alert(\"Please put in a password\");</script>");
	return;
    }
    $salt = createSalt();
    $pass_in = mysqli_real_escape_string($connect, crypt($pass_in, $salt));
    $query = "INSERT INTO Users VALUES(\"" . $user_in . "\", \"" .$pass_in . "\", \"" . $salt . "\");";
    if(mysqli_query($connect, $query))
    {
        setcookie("loggedIn", $user_in, time() + 86400, "./");
	if(isset($_GET["multimedia"]))
        	header("Location: ./add_multimedia.php");
	else if(isset($_GET["climber"]))
		header("Location: ./add_climber.php");
	else
		header("Location: ./home_page.html");
    }
    else
    {
	    echo("<script>alert(\"Sorry, the username is already taken\");</script>");
    }
}
function createSalt()
{
	$salt = "$2a$07$";
	$possibilities = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
	for($i = 0; $i < 23; $i += 1)
		$salt .= $possibilities[rand(0, 51)];
	return $salt;
}

?>
