<?php

$script = $_SERVER['PHP_SELF'];

if (!isset ($_COOKIE["loggedIn"])) {
    header("Location: ./login.html?multimedia=true");
}
else if (isset($_POST["url_input"])) {
	modifyDatabase();
	header("Location: ./multimedia.php");
}
else 
{
    print <<< PAGE
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Add Multimedia</title>
            <link rel="stylesheet" type="text/css" href="../css/add_multimedia.css">
        </head>
	<body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
	</div>
	<div id="form_container">
            <form action="$script" method="post" onsubmit="return validate()">
                <table>
                    <h1>Add link</h1>
                    <thead>
                        <tr>
                            <th>Link URL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text" name="url_input"></td>
                        </tr>
                    </tbody>
		    </table>
		    <input type="submit" value="Submit">
		    </form>
		    <div id="error">
		    </div>
		    </div>
	</body>
	<script>
		function validate()
		{
			let error_box = document.getElementById("error");
			let url_input = document.getElementsByName("url_input")[0];
			if(url_input.value == "" || url_input.value.indexOf("youtube") == - 1 || (url_input.value.length != 41 && url_input.value.length != 43))
			{
				error_box.innerHTML = "<h3>Please enter a valid YouTube URL</h3>";
				return false;
			}
			url_input.value = url_input.value.replace("watch?v=", "embed/");
			return true;
		}
	</script>
    </html>
PAGE;
}

function modifyDatabase() {
    $host = "fall-2019.cs.utexas.edu";
    $user = "cs329e_mitra_jaisimha";
    $pwd = "rump9Easy&suffer";
    $dbs = "cs329e_mitra_jaisimha";
    $port = "3306";

    $connect = mysqli_connect ($host, $user, $pwd, $dbs, $port);
    
    $table = "Links";
    
    $link = strip_tags($_POST["url_input"]);
    if($link == "")
    {
	    echo("<script>alert(\"Please enter a URL\");</script>");
	    return;
    }

    $link = mysqli_real_escape_string($connect, $link);

    $query = "INSERT INTO Links VALUES(\"" . $link . "\");";
    echo("<script>alert(\"" . $query  . "\");</script>");
    if(!mysqli_query($connect,$query))
	    echo("<script>alert(\"It didn't work\");</script>");
    mysqli_close($connect);
}

?>
