<?php

$script = $_SERVER['PHP_SELF'];

if (!isset ($_COOKIE["loggedIn"])) {
    header("Location: ./login.html?climber=true");
}
else if (isset($_POST["name_input"])) {
	modifyDatabase();
    header("Location: ./climbers.php");
}
else 
{
    print <<< PAGE
    <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Add Climber</title>
            <link rel="stylesheet" type="text/css" href="../css/add_climber.css">
        </head>
	<body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
	</div>
	<div id="form_container">
            <form action="$script" method="post" onsubmit="return validate()">
                <table>
                    <h1>Add climber</h1>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Nationality</th>
                            <th>Achievements</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text" name="name_input" size="20"></td>
                            <td><input type="text" name="nationality_input"</td>
                            <td><input type="text" name="achievements_input"</td>
                        </tr>
                    </tbody>
		    </table>
		    <input type="submit" value="Submit">
		    </form>
		    <div id="error">
		    </div>
		    </div>
        </body>
	<script>
	    function validate()
	    {
		let error_box = document.getElementById("error");
		let name_input = document.getElementsByName("name_input")[0];
		let nationality_input = document.getElementsByName("nationality_input")[0];
		let achievements_input = document.getElementsByName("achievements_input")[0];

		if(name_input.value == "" || name_input.value.length > 100)
		{
			error_box.innerHTML = "<h3>Name should be between 1 & 100 characters (inclusive)</h3>";
			return false;
		}
		if(nationality_input.value == "" || nationality_input.value.length > 100)
		{
			error_box.innerHTML = "<h3>Nationality should be between 1 & 100 characters (inclusive)</h3>";
			return false;
		}
		if(achievements_input.value == "" || achievements_input.value.length > 500)
		{
			error_box.innerHTML = "<h3>Achievements should be between 1 & 500 characters (inclusive)</h3>";
			return false;
		}
		return true;
	    }
        </script>
    </html>
PAGE;
}

function modifyDatabase() {
    $host = "fall-2019.cs.utexas.edu";
    $user = "cs329e_mitra_jaisimha";
    $pwd = "rump9Easy&suffer";
    $dbs = "cs329e_mitra_jaisimha";
    $port = "3306";

    $connect = mysqli_connect ($host, $user, $pwd, $dbs, $port);
    
    $table = "Climbers";
    
    $name = strip_tags($_POST["name_input"]);
    $nat = strip_tags($_POST["nationality_input"]);
    $ach = strip_tags($_POST["achievements_input"]);

    if($name == "")
    {
	    echo("<script>alert(\"Please put in a name\");</script>");
	    return;
    }
    if($nat == "")
    {
	    echo("<script>alert(\"Please put in a nationality\");</script>");
	    return;
    }
    if($ach == "")
    {
	    echo("<script>alert(\"Please put in an achievement\");</script>");
	    return;
    }
    
    $name = mysqli_real_escape_string($connect, $name);
    $nat = mysqli_real_escape_string($connect, $nat);
    $ach = mysqli_real_escape_string($connect, $ach);

    $query = "INSERT INTO Climbers VALUES(\"" . $name . "\", \"" . $nat . "\", \"" . $ach . "\");";
    if(!mysqli_query($connect,$query))
	    echo("<script>alert(\"It didn't work\");</script>");
    mysqli_close($connect);
}

?>
