<!DOCTYPE html>
<html lang="en">

<head>
    <title>
        Routes
    </title>
    <meta charset="utf-8">  
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" />   
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/routes.css">
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css">
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
    <script type="text/javascript" src="../js/data.js"></script>
     
</head>

<body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="map">
        <div id="map_marker_description" style="position:relative; top:10px; margin:auto; background-color:white; max-width:800px;  text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;  z-index:998;">
        </div>
    </div>
    <div id="table_container">
	<table id = "table" class="table table-striped">
	    <h1>
                Routes
            </h1>
	    <thead>
	        <tr>
		    <td>
		        Name
		    </td>
		    <td>
		        Grade
		    </td>
		    <td>
		        Crag
		    </td>
		    <td>
		        Country
		    </td>
		    <td>
		        Climbers
		    </td>
		</tr>
	    </thead>
	    <tbody>
		<?php
			$routes = json_decode(file_get_contents("../data_files/routes.json"), true);
			for($i = 0; $i < count($routes); $i += 1)
			{
				echo("<tr>");
				echo("<td>" . $routes[$i]["Name"] . "</td>");
				echo("<td>" . $routes[$i]["Grade"] . "</td>");
				echo("<td>" . $routes[$i]["Crag"] . "</td>");
				echo("<td>" . $routes[$i]["Country"] . "</td>");
				echo("<td>");
				echo("<ul>");
				sort($routes[$i]["Ascensionists"]);
				for($j = 0; $j < count($routes[$i]["Ascensionists"]); $j += 1)
				{
					echo("<li>" . $routes[$i]["Ascensionists"][$j] . "</li>");
				}
				echo("</ul>");
			        echo("</td>");
				echo("</tr>");
			}
                ?>
	    </tbody>
	</table>
        <p>Source: <a href="https://www.99boulders.com/hardest-sport-climbs">99 Boulders</a></p>
    </div>
    <br><br><br>
</body>
<script>
    $(document).ready(function(){
	    $('#table').dataTable();
    });
    var options = {
        center: [15, 0],
        zoom: 2,
        maxBounds: new L.LatLngBounds(new L.LatLng(-60, -180), new L.LatLng(90, 180))
    };

    var map = L.map('map', options);

    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        noWrap: true,
        attribution: "<a href=\"home_page.html\">New Heights</a>"
    }).addTo(map);
    function main() {
	for (var i = 0; i < routesJSON.length; i += 1) {
	    let coords = routesJSON[i]["Coordinates"];
            let place_name = routesJSON[i]["Crag"] + ", " + routesJSON[i]["Country"];
	    // L.marker(coords).addTo(map).on('click', function(){alert(place_name);});
	    L.marker(coords).addTo(map).on('click', function(){document.getElementById("map_marker_description").innerHTML = "<h1>" + place_name + "</h1>";});
        }
    }
    main()
</script>

</html>
