<!DOCTYPE html>
<html lang="en">

<head>
    <title>
	Multimedia
    </title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/multimedia.css">
</head>

<body>
    <div id="header_and_nav_bar">
        <table>
            <tbody>
                <tr>
                    <td>
                        <a href="./multimedia.php">
								Multimedia
							</a>
                    </td>
                    <td>
                        <a href="./climbers.php">
								Climbers
							</a>
                    </td>
                    <td>
                        <a href="./home_page.html">
                            <img src="../images/logo.png" height=100>
                        </a>
                    </td>
                    <td>
                        <a href="./routes.php">
								Routes
							</a>
                    </td>
                    <td>
                        <a href="./contact_us.html">
								Contact Us
							</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="videos">
        <table>
	    <tbody>
		<?php
			    $host = "fall-2019.cs.utexas.edu";
			    $user = "cs329e_mitra_jaisimha";
			    $pwd = "rump9Easy&suffer";
			    $dbs = "cs329e_mitra_jaisimha";
			    $port = "3306";
			    $connect = mysqli_connect($host, $user, $pwd, $dbs, $port);

			    if(!isset($_GET["page"]))
				    $page = 0;
			    else
				    $page = (int)($_GET["page"]);
			    $num_entries = mysqli_query($connect, "SELECT COUNT(*) FROM Links;")->fetch_row()[0];
			    if($page < 0 || $page * 15 > $num_entries)
				    $page = 0;

			    $query = "SELECT * FROM Links ORDER BY Link limit " . $page * 15 . ", 15;";
			    $result = mysqli_query($connect, $query);
			    echo("<tr>");
			    $count = 0;
			    while($row = $result->fetch_row())
			    {
				    $count += 1;
				print('<td><iframe width="300" src="' . $row[0] . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>');
				    if($count % 3 == 0)
					    echo("</tr><tr>");
			    }
			    echo("</tr>");
			    echo("<tr><td colspan=\"3\">");

			    $script = $_SERVER["PHP_SELF"];
			    if($page > 0)
			    {
				    $script = $_SERVER["PHP_SELF"] . "?page=" . strval($page - 1);
				    echo("<input type=\"button\" onclick=\"location.href='" . $script . "';\" value=\"Previous Page\">");
			    }
			    if($page < ceil($num_entries / 15) - 1)    
			    {
				    $script = $_SERVER["PHP_SELF"] . "?page=" . strval($page + 1);
				    echo("<input type=\"button\" onclick=\"location.href='" . $script . "';\" value=\"Next Page\">");
			    }
			    echo("</td></tr>");
		?>
            </tbody>
        </table>
        <br><br>
        <p>Don't see your favorite video? Add it <a href="./add_multimedia.php">here</a>.</p>
    </div>
    <div>
        <br><br><br>
    </div>
    <br><br><br>
</body>

</html>
