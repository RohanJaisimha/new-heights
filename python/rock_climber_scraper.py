import requests
import bs4
import json
import re
import unidecode


def getHTML(url):
    fout = open("./rock_climbers_raw.txt", 'w')
    fout.write(unidecode.unidecode(requests.get(url).text))
    fout.close()


def cleanData():
    fin = open("./rock_climbers_raw.txt", 'r')
    x = fin.read()
    soup = bs4.BeautifulSoup(x, "html.parser")
    fin.close()

    data = []
    list_items = soup.find_all("ul")
    for i in range(1, len(list_items) - 31):
        data += list_items[i].text.split("\n")

    climbers = []
    for line in data:
        line = unidecode.unidecode(line)
        climber_info = {}
        line = re.sub("\(.{4,20}\)", '', line.strip(), count=1).strip()
        line = re.sub("\[.{1,3}\]", '', line, count=1)
        line = line.split("  ")
        for j in range(len(line)):
            line[j] = line[j].strip()
        if(len(line) == 2):
            line[1] = line[1].split(",", 1)
            for j in range(len(line[1])):
                line[1][j] = line[1][j].strip()
            if(len(line[1]) == 2):
                line[1][1] = line[1][1][0].upper() + line[1][1][1:]
                climber_info["Name"] = line[0]
                climber_info["Nationality"] = line[1][0]
                climber_info["Achievments"] = line[1][1]
                climbers.append(climber_info)

    return climbers


def writeToFile(data, f_name):
    fout = open(f_name, 'w')
    json.dump(data, fout, indent = 4)
    fout.close()
    print("There were", len(data), "people.")


def main():
    url = "https://en.wikipedia.org/wiki/List_of_climbers_and_mountaineers"
    # getHTML(url)
    data = cleanData()
    writeToFile(data, "../data_files/rock_climbers.json")


if(__name__ == "__main__"):
    main()
