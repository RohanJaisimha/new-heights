import json

def jsonify(data):
    fout = open("../data_files/links.json", 'w')
    json.dump(data, fout, indent=4)
    fout.close()

def getData(f_name):
    fin = open(f_name, 'r')
    data = fin.readlines()
    data = [i.strip() for i in data]
    fin.close()
    return data

def main():
    jsonify(getData("../data_files/links_raw.txt"))

if(__name__ == "__main__"):
    main()
