import requests
import bs4
import json
import re
import unidecode


def getHTML(url):
    fout = open("./routes_raw.txt", 'w')
    fout.write(requests.get(url).text)
    fout.close()


def getCoords(crag, country):
    url = "https://nominatim.openstreetmap.org/search?format=json&limit=1&q="
    x = requests.get(url + crag + ", " + country).json()
    try:
        return x[0]["lat"], x[0]["lon"]
    except:
        x = requests.get(url + country).json()
        return x[0]["lat"], x[0]["lon"]


def cleanData():
    fin = open("./routes_raw.txt", 'r')
    x = fin.read()
    soup = bs4.BeautifulSoup(x, "html.parser")
    fin.close()

    routes = []
    tds = soup.findAll("td")
    for i in range(0, len(tds), 5):
        d = {}
        d["Name"] = unidecode.unidecode(tds[i].text)
        d["Grade"] = unidecode.unidecode(tds[i + 1].text)
        d["Crag"] = unidecode.unidecode(tds[i + 2].text)
        d["Country"] = unidecode.unidecode(tds[i + 3].text)
        d["Ascensionists"] = unidecode.unidecode(tds[i + 4].text).split(", ")
        d["Coordinates"] = getCoords(d["Crag"], d["Country"])
        routes.append(d)
    return routes


def writeToFile(data, f_name):
    fout = open(f_name, 'w')
    json.dump(data, fout, indent=4)
    fout.close()
    print("There were", len(data), "routes.")


def main():
    url = "https://www.99boulders.com/hardest-sport-climbs"
    # getHTML(url)
    data = cleanData()
    writeToFile(data, "../data_files/routes.json")


if(__name__ == "__main__"):
    main()
